<?php
function tentukan_nilai($number)
{
    $output = "";
    if($number >= 98 && $number < 100){
        $output .= "Sangat baik";
    }else if($number >=76 && $number < 98){
        $output .= "Baik";
    }else if($number >=67 && $number < 76){
        $output .= "Cukup";
    }else{
        $output .= "Kurang";
    }
    return $output . "<br>";
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>